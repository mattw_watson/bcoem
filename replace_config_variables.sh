#!/bin/sh -x

SETUP=$1
DB_NAME=$2
DB_USERNAME=$3
DB_PASSWORD=$4
SMTP_PASSWORD=$5

#sed -e "s/^\$username = \'\'/\$username = \'${DB_USERNAME}\'/" -e "s/^\$password = \'\'/\$password = \'${DB_PASSWORD}\'/" -e "s/^\$database = \'\'/\$database = \'${DB_NAME}\'/" -e "s/^\$installation_id = \'\'/\$installation_id = \'${DB_NAME}\'/" -e "s/^\$setup_free_access = FALSE/\$setup_free_access = ${SETUP}/"
sed "s/^\$username = ''/\$username = '${DB_USERNAME}'/" |\
sed "s/^\$password = ''/\$password = '${DB_PASSWORD}'/" |\
sed "s/^\$database = ''/\$database = '${DB_NAME}'/" |\
sed "s/^\$installation_id = ''/\$installation_id = '${DB_NAME}'/" |\
sed "s/^\$smtp_password = \"\"/\$smtp_password = \"${SMTP_PASSWORD}\"/" |\
sed "s/^\$setup_free_access = FALSE/\$setup_free_access = ${SETUP}/"
