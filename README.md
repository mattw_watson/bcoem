This repository contains the instructions and scripts for setting up a competition on HostMines under nswhomebrewing.org.

The source code we use is in the nswhb branch of the fork of bcoem here: https://github.com/mattwwatson/brewcompetitiononlineentry/tree/nswhb

* If github says the upstream is ahead, sync the new commits down so we have the latest version to use. This can be done later after setting a site up and rerun the bitbucket custom pipeline to get the latest changes.
* The nswhb branch has some minor changes we made for NSW in the 4 unique commits on that branch.

1. use AEST as our timezone, not ChST
2. custom indents for 3422 labels
3. Talk about "containers" not "bottles"
4. Some changes to packaging and payment text.


* What we need to set up a new competition

1. name of the comp subdomain, e.g. XXXXXXX.nswhomebrewing.org (name will all be lower case)
2. who should receive emails for organiser@XXXXXXX.nswhomebrewing.org?
3. If wanted, an email for steward@XXXXXXX.nswhomebrewing.org

Note: The scripts assume the dbname dbusername and path from the name of the deployment environment in bitbucket pipelines

* How to set up a new competition *

First, create the database

* Login to https://www.hostplax.com/client/clientarea.php?action=productdetails&id=74 - now https://www.hostmines.com/client/clientarea.php?action=productdetails&id=74
* Go to MySQL databases, create a new user named after the comp , e.g. nswhomeb_esb2020 - give it a new secure password and store that somewhere secure (like 1Password)
* Create a database with the same name
* Assign the new user to the new database and assign it all privileges.
* Navigate to Domains -> Create New
* Domain should be similar to db name, e.g. esb2020.nswhomebrewing.org, override document root to be /competitions/esb2020 (for example)
* navigate to email forwarders
* Add aliases for organiser@esb2020.nswhomebrewing.org (and anything else the comp wants)
* Under bitbucket-pipelines.yml, create a new custom pipeline (copy an old one) for the comp, replace the names with the name for the comp - deployment: is esp important to get right!
* Go to https://bitbucket.org/mattw_watson/bcoem/admin/addon/admin/pipelines/deployment-settings and add a deployment environment for the new comp - this must be the same unique part that you used for the DB name, user and competitions path e.g. (/competitions/esb2019, nswhomeb_esb2019 -> esb2019)
* Set the DB_PASSWORD for the database we created above as a secured variable within the environment:
** DB_PASSWORD (value - what you generated earlier) MARK AS SECURED
* Commit the bitbucket-pipelines.yml and push it back to bitbucket
* Navigate to https://bitbucket.org/mattw_watson/bcoem/commits/master and `run pipeline` - choose the custom pipeline for the new comp
** You can rerun the deployment to get updates from https://github.com/mattwwatson/brewcompetitiononlineentry/tree/nswhb or rerun the whole custom pipeline

But we don't have the database actually set up yet, so don't hit the subdomain yet!

* Navigate to https://github.com/mattwwatson/brewcompetitiononlineentry/tree/nswhb/sql and click on the latest bcoem_baseline_X.Y.Z.sql file
* Select "RAW" and copy/paste (or save/load) the contents to your favourite editor
* Search and replace (in emacs this is M-%) all instances of "baseline_" with nothing, e.g. ""
* Copy the new contents (Ctrl-C/Meta-C)
* Go to PHPMyadmin
* On the left select the database (+) entry you've just added
* Click on SQL at the top
* paste the whole contents of the sql file you edited earlier (withthe prefixes removed) into the textarea and execute it (hit "go" button)
* Click on the name of the data base again - there should now be a bunch of tables there...
* Go to the users table of the new comp database
* edit the single entry and replace the email address with the email address of your organiser -> go
* Go to the site and trigger the reset password flow to set the password. The answer to security question is `pabst`


* For a competition with paypal, you need to specify the paypal instant payment notification link - turn it on in https://XXXX.nswhomebrewing.org//index.php?section=admin&go=preferences then log into the paypal account and specify the return/nitification link as per the instructions in the prefs for that section

Other Notes:

* The SFTP port was 2200, but it seems to have changed to 37980 - this is set in the bitbucket_pipelines.yml file. - I discovered the new value by inspecting the config under cpanel->FTP Accounts -> nswhomeb -> configure FTP Client -> it says port 21, but if you download the duck sftp config file and look in it you can see the actual config file - youn can test with. `sftp -v -P 37980 nswhomeb@ftp.nswhomebrewing.org ttt`
* There is a problem with custom styles where they seem to get the same brewStyleNumber - go to phpMyAdmin and edit them manually to make sure they all have different brewStyleNum. brewStyleGroup and brewStyleCategory can be anything
* CAPTCHA seems to flat out not work - I believe in the past Iw as ignorring the output on page submit, but since getting the latest from https://github.com/geoffhumphrey/brewcompetitiononlineentry can't get CAPTCHA to work